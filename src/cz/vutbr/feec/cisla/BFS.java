package cz.vutbr.feec.cisla;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.TreeSet;

public class BFS {
	public void vypisTahy(HraciPole zadani) {

		HashSet<HraciPole> closed = new HashSet<HraciPole>();
		LinkedList<HraciPole> open = new LinkedList<HraciPole>();

		closed.add(zadani);
		open.add(zadani);

		while (!open.isEmpty()) {
			HraciPole tmp = open.removeFirst();
			if (tmp.jeRiesenie()) {
				System.out.println("Riesenie najdene!");
				System.out.println(tmp);
				return;
			}
			for (int t = 1; t <= 4; t++) {
				HraciPole nove = tmp.klonujAPohni(t);
				if (nove != null && !closed.contains(nove)) {
					closed.add(nove);
					open.addLast(nove);
				}
			}
		}
		System.out.println("Riesenie neexistuje!");
	}
}
